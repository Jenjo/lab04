package it.unibo.oop.lab04.robot.arms.RobotWithTwoArms;

import it.unibo.oop.lab04.robot.arms.basicArm.BasicArm;
import it.unibo.oop.lab04.robot.base.BaseRobot;

public class RobotWithTwoArms extends BaseRobot implements RobotWithArms  {

	private final static double CONSUME_WITH_OBJ = 1;
	private final static int OBJECT = 1;
	
	private final BasicArm arm1;
	private final BasicArm arm2; 
	private int item; 
	/**
	 * 
	 * @param robotName String wich rapresent name of robot
	 */
	public RobotWithTwoArms(final String robotName) {
		super(robotName);
		this.arm1 = new BasicArm("Braccio1");
		this.arm2 = new BasicArm("Braccio2");
		this.item = 0;
	}
	/**
	 * Se uno dei bracci è libero (item < 2) il robot raccoglie l'oggetto
	 * con uno dei bracci e il numero di oggetti trasportati (item) viene incrementato,
	 * altrimenti il robot non fà nulla.
	 * 
	 * If once of arms is free, (item < 2) the robot pick up the object with once of free arms,
	 * and number of object increases, otherwise robot doesn't do anything
	 */
	@Override
	public boolean pickUp() {
		
		if(this.item < 2 && this.getBatteryLevel() > BasicArm.getConsuptionForPickUp()) {
			if(!this.arm1.isGrabbing()) {
				this.arm1.pickUp();
			} else {
				this.arm2.pickUp();
			}
			return this.consume(OBJECT, BasicArm.getConsuptionForPickUp());
		}
		return false;
	}
	/**
	 * Se il robot ha almeno un oggetto in mano (item > 0), ne lascia a terra uno 
	 * e viene decrementato il numero di oggetti trasportati, altrimenti il robot
	 * non fa nulla
	 * 
	 * If robot has at least one object, (item > 0) it drop out once
	 * and number of object decreases, otherwise robot doesn't do anything
	 */
	@Override
	public boolean dropDown() {
		if(this.item > 0 && this.getBatteryLevel() > BasicArm.getConsuptionForDropDown()) {
			if(this.arm1.isGrabbing()) {
				this.arm1.dropDown();
			} else {
				this.arm2.dropDown();
			}
			return this.consume(-OBJECT, BasicArm.getConsuptionForDropDown());
		}
		return false;
	}
	/**
	 * @return The number of item carried
	 */
	@Override
	public int getItemsCarried() {
		return this.item;
	}
	/**
	 * 
	 * If robot transports some object, the consume of the battery is greater 
	 */
	@Override
	 public void consumeBatteryForMovement() {
       this.consumeBattery(MOVEMENT_DELTA_CONSUMPTION + (this.item * CONSUME_WITH_OBJ));
    }
	/**
	 *
	 * this method increases or decreases number of objects carried, 
	 * and decreases battery energy 
	 *
	 * @return always true
	 */
	private boolean consume(final int obj, final double energy) {
		this.item += obj;
		this.consumeBattery(energy);
		return true;
	}

}
