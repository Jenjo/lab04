package it.unibo.oop.lab04.robot.arms.RobotWithTwoArms;

import it.unibo.oop.lab04.robot.base.Robot;

public interface RobotWithArms extends Robot {
	/**
	 * pick up an object
	 * @return true if arm grabs object, false otherwise
	 */
	boolean pickUp();
	/**
	 * drop down the object which was grabbed 
	 * @return true if arm drop down object, false otherwise
	 */
	boolean dropDown();
	/**
	 * 
	 * @return number of item transported by robot
	 */
	int getItemsCarried();
}
