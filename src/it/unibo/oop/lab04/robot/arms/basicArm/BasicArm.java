package it.unibo.oop.lab04.robot.arms.basicArm;

public class BasicArm {
	
	
	private final static double PICK_UP = 0.1;
	private final static double DROP_DOWN = 0.2;
	private final String name;
	private boolean grab;

	/**
	 * @param name : Name of the arm
	 */
	public BasicArm(final String name) {
		this.name = name;
		this.grab = false;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}
	/**
	 *
	 * @return true if arm is grabbing an object, false otherwise
	 */
	public boolean isGrabbing() {
		return this.grab;
	}
	/**
	 * If arm is free, pick up the object
	 */
	public void pickUp() {
		if(!this.grab) {
			this.grab = true;
		}
	}
	/**
	 * if the arm has an object, the arm drop down it
	 */
	public void dropDown() {
		if(this.grab) {
			this.grab = false;
		}
	}
	/**
	 * 
	 * @return quantity of energy necessary to pick up an object
	 */
	static public double getConsuptionForPickUp() { 
		return PICK_UP;
	}
	/**
	 * 
	 * @return quantity of energy necessary to drop down an object
	 */
	static public double getConsuptionForDropDown() { 
		return DROP_DOWN;
	}

	@Override
	public String toString() {
		return "BasicArm [name=" + this.name + ", isGrabbing()=" + isGrabbing()
				+ ", getConsuptionForPickUp()=" + getConsuptionForPickUp() + ", getConsuptionFordropDownp()="
				+ getConsuptionForDropDown() + "]";
	}
	
}
