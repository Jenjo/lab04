package it.unibo.oop.lab04.robot.components;

public class Navigator extends AbstractComponent {

	final private static double NAVIGATOR_CONSUME = 0.2;
	
	public Navigator(final String name) {
		super(name);
	}
	public double getNavigatorConsume(){
		return NAVIGATOR_CONSUME;
	}
}
