package it.unibo.oop.lab04.robot.components;

public class AbstractComponent implements Component {

	
	final private String name;
	private boolean switching;
	
	/**
	 * @param name
	 * @param switching
	 */
	public AbstractComponent(final String name) {
		this.name = name;
		this.switching = false;
	}

	@Override
	public void switchOn() {
		if(!this.switching) {
			this.switching = true;
		}

	}

	@Override
	public void switchOff() {
		if(this.switching) {
			this.switching = false;
		}
	}

	@Override
	public boolean isSwitch() {
		// TODO Auto-generated method stub
		return this.switching;
	}
	
	public String getName() {
		return this.name;
	}
}
