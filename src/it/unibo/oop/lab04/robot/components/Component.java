package it.unibo.oop.lab04.robot.components;

public interface Component {
	/**
	 * Attiva il componente
	 */
	void switchOn();
	/**
	 * Disattiva il componente
	 */
	void switchOff();
	/**
	 * 
	 * @return status of component
	 */
	boolean isSwitch();
}
