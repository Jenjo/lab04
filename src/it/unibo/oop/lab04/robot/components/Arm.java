package it.unibo.oop.lab04.robot.components;

public class Arm extends AbstractComponent {

    final static private double PICK_UP = 0.1;
    final static private double DROP_DOWN = 0.2;

    private boolean grab;

    /**
     * @param name
     * @param grab
     * @param switching
     */
    public Arm(final String name) {
        super(name);
        this.grab = false;
    }

    public boolean isGrabbing() {
        return this.grab;
    }

    public void pickUp() {
        if (!this.grab && this.isSwitch()) {
            this.grab = true;
        }
    }

    public void dropDown() {
        if (this.grab && this.isSwitch()) {
            this.grab = false;
        }
    }

    static public double getConsuptionForPickUp() {
        return PICK_UP;
    }

    static public double getConsuptionFordropDownp() {
        return DROP_DOWN;
    }

    public String toString() {
        return "BasicArm [name=" + this.getName() + ", isGrabbing()=" + isGrabbing() + ", getConsuptionForPickUp()="
                + getConsuptionForPickUp() + ", getConsuptionFordropDownp()=" + getConsuptionFordropDownp() + "]";
    }
}
