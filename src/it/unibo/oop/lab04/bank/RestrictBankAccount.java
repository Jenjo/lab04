package it.unibo.oop.lab04.bank;

public class RestrictBankAccount extends AbstractBankAccount {
	
	private static final double TRANSACTION_FEE = 0.1;

	public RestrictBankAccount(final int usrID, final double balance) {
		super(usrID, balance);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected double computeFee() {
		// TODO Auto-generated method stub
		return (TRANSACTION_FEE * this.getNTransactions()) + MANAGEMENT_FEE;
	}

	@Override
	protected boolean isWithdrawAllowed(final double feeAmount) {
		// TODO Auto-generated method stub
		return this.getBalance() > feeAmount;
	}

}
