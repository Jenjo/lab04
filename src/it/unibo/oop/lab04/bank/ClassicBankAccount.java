package it.unibo.oop.lab04.bank;

public class ClassicBankAccount extends AbstractBankAccount {

	public ClassicBankAccount(final int usrID, final double balance) {
		super(usrID, balance);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected double computeFee() {
		// TODO Auto-generated method stub
		return MANAGEMENT_FEE;
	}

	@Override
	protected boolean isWithdrawAllowed(final double feeAmount) {
		// TODO Auto-generated method stub
		return true;
	}

}
