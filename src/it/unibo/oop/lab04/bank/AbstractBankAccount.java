package it.unibo.oop.lab04.bank;

public abstract class AbstractBankAccount implements BankAccount {
	
	public static final double ATM_TRANSACTION_FEE = 1;
    public static final double MANAGEMENT_FEE = 5;

    private double balance;
    private int nTransactions;
    private final int usrID;

    public AbstractBankAccount(final int usrID, final double balance) {
        this.usrID = usrID;
        this.balance = balance;
        this.nTransactions = 0;
    }

    protected boolean checkUser(final int id) {
        return this.usrID == id;
    }

	@Override
	public void computeManagementFees(final int usrID) {
		final double feeAmount = computeFee();
		 if (checkUser(usrID) && isWithdrawAllowed(feeAmount)) {
	            balance -= feeAmount;
	            nTransactions = 0;
	     }
	}

	@Override
	public void deposit(final int usrID, final double amount) {
        this.transactionOp(usrID, amount);
    }
	@Override
    public void depositFromATM(final int usrID, final double amount) {
        this.deposit(usrID, amount - ATM_TRANSACTION_FEE);
    }	
	@Override
	public void withdraw(final int usrID, final double amount) {
		if(isWithdrawAllowed(amount)) {
			this.transactionOp(usrID, -amount);
		}
    }
	@Override
    public void withdrawFromATM(final int usrID, final double amount) {
        this.withdraw(usrID, amount + SimpleBankAccount.ATM_TRANSACTION_FEE);
    }
	
	@Override
	public double getBalance() {
		// TODO Auto-generated method stub
		return this.balance;
	}

	@Override
	public int getNTransactions() {
		// TODO Auto-generated method stub
		return this.nTransactions;
	}
	
	public int getUsrID() {
		// TODO Auto-generated method stub
		return this.usrID;
	}
	
	protected abstract double computeFee();
	
	protected abstract boolean isWithdrawAllowed(final double feeAmount);
	
	protected void incTransactions() {
        this.nTransactions++;
    }

    protected void resetTransactions() {
        this.nTransactions = 0;
    }

    protected void setBalance(final double amount) {
        this.balance = amount;
    }
	
	private void transactionOp(final int usrID, final double amount) {
        if (checkUser(usrID)) {
            this.balance += amount;
            this.incTransactions();
        }
    }
	
}
